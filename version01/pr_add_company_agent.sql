create or replace PROCEDURE "PR_ADD_COMPANY_AGENT" (p_user IN VARCHAR) AS
    CURSOR data_cursor
    IS
SELECT 
    invc_details.inx_auto_key,
    invc_head.invc_number,
    invc_details.agn_auto_key AS agn_invc,
CASE 
    WHEN company.agn_auto_key IS NOT NULL THEN company.agn_auto_key   
    ELSE 1 
    END AS agn_cmp
FROM
    invc_details   
JOIN invc_head ON invc_details.inh_auto_key = invc_head.inh_auto_key
JOIN company ON invc_head.cmp_auto_key = company.cmp_auto_key
WHERE (1=1)
AND ((invc_details.agn_auto_key IS NULL AND company.agn_auto_key IS NOT NULL) OR
(invc_details.agn_auto_key <> company.agn_auto_key) OR (invc_details.agn_auto_key IS NOT NULL AND company.agn_auto_key IS NULL))
AND company.cmp_auto_key <> 1
AND invc_details.scz_auto_key IN (1, 2, 3) 
AND invc_details.invoice_date >= sysdate - 30;

BEGIN      
pg_database_login.pr_database_login(p_user);
FOR table_data IN data_cursor
        LOOP
          UPDATE invc_details
          SET 
            closed_update = 'T',
            invc_details.agn_auto_key = table_data.agn_cmp
          WHERE inx_auto_key = table_data.inx_auto_key;           
        END LOOP;
      COMMIT;     
pg_database_login.pr_database_logout(p_user);
END;

